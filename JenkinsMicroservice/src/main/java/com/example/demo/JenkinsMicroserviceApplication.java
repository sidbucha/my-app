package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JenkinsMicroserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(JenkinsMicroserviceApplication.class, args);
	}

}
