package com.example.demo;

import org.springframework.boot.autoconfigure.SpringBootApplication;


public class StudentBean {
	private String name;
	private String roolno;
	private String age;
	public StudentBean(String name, String roolno, String age) {
		super();
		this.name = name;
		this.roolno = roolno;
		this.age = age;
	}
	public StudentBean() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getRoolno() {
		return roolno;
	}
	public void setRoolno(String roolno) {
		this.roolno = roolno;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	

}
