package com.example.demo;

import java.net.URI;
import java.net.URISyntaxException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import junit.framework.Assert;

@RunWith(SpringRunner.class)
@SpringBootTest
public class JenkinsMicroserviceApplicationTests {

	@Test
	public void check() throws URISyntaxException {
		
		RestTemplate restTemplate = new RestTemplate();
		final String baseUrl = "http://localhost:" + 8095 + "/getAll";
		URI uri = new URI(baseUrl);
		String result1 = restTemplate.getForObject(uri, String.class);
		Assert.assertEquals(
				"[{\"name\":\"Sid\",\"roolno\":\"12\",\"age\":\"15\"},{\"name\":\"Sid\",\"roolno\":\"12\",\"age\":\"15\"}]",
				result1);
		
	}

}
